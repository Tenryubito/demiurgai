import pandas as pd

from Src.Entities.GameObjectEntity import GameObjectEntity
from Src.Entities.Tag import Tag


def load_tags_from_directory(directory_name):
    df = pd.read_csv(f'{directory_name}/tags.csv', delimiter=';', header=None)
    tags = []
    for row in df.values:
        tag_parts = row[0].split('_')
        tags.append(Tag(tag_parts[0], tag_parts[1]))

    return tags


def load_game_objects_from_directory(directory_name, tags):
    df = pd.read_csv(f'{directory_name}/game_objects.csv', delimiter=';')
    game_objects = []
    for row in df.values:
        try:
            tags_id = row[1].split(', ')
        except:
            tags_id = []

        demiurg_object = GameObjectEntity(row[0])
        demiurg_object = add_tags_from_ids(demiurg_object, tags, tags_id)
        game_objects.append(demiurg_object)

    return game_objects


def add_tags_from_ids(demiurg_object, tags, tags_id):
    for tag_id in tags_id:
        if tag_id != '':
            tags_with_id = [tag for tag in tags if tag.id() == tag_id]
            if len(tags_with_id) > 0:
                demiurg_object.add_tag(tags_with_id[0])
            else:
                print(f'Loading: Tag not found: {tag_id}')

    return demiurg_object
