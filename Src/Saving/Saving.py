import os

from Src.Entities.AprioriRule import margin_of_error


def write_to_file(file_name, text):
    file = open(file_name, "w")
    file.write(text)
    file.close()


def reformat_list(list_to_reformat):
    text = f"{list_to_reformat}"
    return text[1:-1]


def save_experiment(seed, tags, rule_parameters, game_objects, expected_rules, rules, noise_cover, min_supp, min_conf,
                    filtered_trivial_rules=None, filtered_coupled_rules=None):
    save_experiment_with_filtering(seed, tags, rule_parameters, game_objects, expected_rules, rules, noise_cover,
                                   min_supp, min_conf)

    directory_path = create_directory_path(seed, game_objects, expected_rules, noise_cover)

    if filtered_trivial_rules is not None:
        save_rules_with_name(directory_path, "rules_filtered_trivial", min_supp, min_conf, filtered_trivial_rules,
                             len(game_objects))

    if filtered_coupled_rules is not None:
        save_rules_with_name(directory_path, "rules_filtered_coupled", min_supp, min_conf, filtered_coupled_rules,
                             len(game_objects))


def save_experiment_with_filtering(seed, tags, rule_parameters, game_objects, expected_rules, rules, noise_cover,
                                   min_supp, min_conf):
    directory_path = create_directory_path(seed, game_objects, expected_rules, noise_cover)

    save_game_objects_tags_and_rules(directory_path, game_objects, tags, min_supp, min_conf, rules, "rules_filtered")

    write_to_file(f"{directory_path}/rule_parameters.csv", f"{rule_parameters}")
    save_expected_rules(directory_path, expected_rules)


def create_directory_path(seed, game_objects, expected_rules, noise_cover):
    base_directory = f"OUTPUT"
    return create_brute_sub_directory(base_directory, seed, game_objects, expected_rules, noise_cover)


def create_brute_sub_directory(base_directory, seed, game_objects, expected_rules, noise_cover):
    directory_path = f"{base_directory}/Seed_{seed}_Objects_{len(game_objects)}_Rules_{len(expected_rules)}_Noise_{noise_cover}"
    return create_directory_path_if_not_exists(directory_path)


def create_directory_path_if_not_exists(directory_path):
    if not os.path.exists(directory_path):
        os.mkdir(directory_path)
    return directory_path


def remove_directory(directory_path):
    os.rmdir(directory_path)


def save_game_objects_tags_and_rules(directory_path, game_objects, tags, min_supp, min_conf, rules, rules_file_name):
    save_tags_and_game_objects(directory_path, game_objects, tags)
    save_rules_with_name(directory_path, rules_file_name, min_supp, min_conf, rules, len(game_objects))


def save_tags_and_game_objects(directory_path, game_objects, tags):
    create_directory_path_if_not_exists(directory_path)
    save_tags(directory_path, tags)
    save_game_objects(directory_path, game_objects)


def save_tags(directory_path, tags):
    formatted_tags = reformat_list(tags).replace(", ", "\n")
    write_to_file(f"{directory_path}/tags.csv", formatted_tags)


def save_game_objects(directory_path, game_objects):
    formatted_game_objects = reformat_list(game_objects).replace(": ", ";").replace("[", "").replace("]", "")
    write_to_file(f"{directory_path}/game_objects.csv", f"ID;Tags{formatted_game_objects}")


def save_expected_rules(directory_path, expected_rules):
    formatted_expected_rules = reformat_list(expected_rules).replace(" -> ", ";").replace(" supp: ", ";").replace(
        " conf: ", ";").replace("[", "").replace("]", "").replace(", \n", "\n")
    write_to_file(f"{directory_path}/expected_rules.csv",
                  f"Premise;Recommendation;Support;Confidence{formatted_expected_rules}")


def save_rules_with_name(directory_path, name, min_supp, min_conf, rules, game_objects_count):
    formatted_rules = reformat_rules(rules, game_objects_count)
    write_to_file(f"{directory_path}/{name}.csv",
                  f"Premise;Recommendation;Support ({min_supp});Confidence ({min_conf});Lift;Conv;EBM\n{formatted_rules}")


def reformat_rules(rules, game_objects_count):
    rules = [f"{list(rule.lhs)};{list(rule.rhs)};{format_indicators(rule, game_objects_count)}^" for rule in rules]
    formatted_rules = reformat_list(rules)[1:-2].replace("'", "").replace("^\", \"", "\n").replace("[", "").replace("]",
                                                                                                                    "")
    return formatted_rules


def format_indicators(rule, game_objects_count):
    supp = "{0:.3f}".format(rule.support)
    conf = "{0:.3f}".format(rule.confidence)
    lift = "{0:.3f}".format(rule.lift)
    conv = "{0:.3f}".format(rule.conviction)
    ebm = margin_of_error(rule.support * game_objects_count, rule.confidence)

    return "{};{};{};{};{}".format(supp, conf, lift, conv, ebm)


def save_with_various_constraints(seed, game_objects, expected_rules, noise_cover, rules, supp_thresholds,
                                  conf_thresholds, filtered_trivial_rules, filtered_coupled_rules):
    directory_path = create_directory_path(seed, game_objects, expected_rules, noise_cover)

    save_vith_various_constraints_on_path(directory_path, supp_thresholds, conf_thresholds, rules,
                                          filtered_trivial_rules, filtered_coupled_rules)


def save_vith_various_constraints_on_path(directory_path, supp_thresholds, conf_thresholds,
                                          rules, filtered_trivial_rules, filtered_coupled_rules):
    save_various_constraints_on_thresholds_with_sufix(directory_path, "", rules, supp_thresholds, conf_thresholds)
    save_various_constraints_on_thresholds_with_sufix(directory_path, "_filtered_trivial", filtered_trivial_rules,
                                                      supp_thresholds,
                                                      conf_thresholds)
    save_various_constraints_on_thresholds_with_sufix(directory_path, "_filtered_coupled", filtered_coupled_rules,
                                                      supp_thresholds,
                                                      conf_thresholds)


SUPP_THRESHOLDS = [0.001, 0.002, 0.003, 0.004, 0.005, 0.0075, 0.01, 0.02, 0.03, 0.04, 0.05,
                   0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.15, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
CONF_THRESHOLDS = [0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1]


def prepare_thresholds_for(min_support, min_confidence):
    supp_thresholds = [supp for supp in SUPP_THRESHOLDS if supp >= min_support]
    conf_thresholds = [conf for conf in CONF_THRESHOLDS if conf >= min_confidence]
    return supp_thresholds, conf_thresholds


def save_various_constraints_with_sufix(directory_path, sufix, rules, min_support, min_confidence):
    supp_thresholds, conf_thresholds = prepare_thresholds_for(min_support, min_confidence)

    save_various_constraints_on_thresholds_with_sufix(directory_path, sufix, rules, supp_thresholds, conf_thresholds)


def save_various_constraints_on_thresholds_with_sufix(directory_path, sufix, rules, supp_thresholds, conf_thresholds):
    output_rules = format_various_constraints(rules, supp_thresholds, conf_thresholds)
    write_to_file(f"{directory_path}/comparison_with_various_constraints{sufix}.csv", f"{output_rules}")


def format_various_constraints(rules, supp_thresholds, conf_thresholds):
    output = f"{supp_thresholds}"[1:-1].replace(", ", ";")
    output = f"conf\\supp;{output}\n"
    for conf in conf_thresholds:
        output += f"{conf};"
        for supp in supp_thresholds:
            output += f"{len([rule for rule in rules if rule.support >= supp and rule.confidence >= conf])};"
        output += '\n'
    return output


def save_data_with_rules(directory_path, game_objects, tags, min_supp, min_conf, rules):
    save_game_objects_tags_and_rules(directory_path, game_objects, tags, min_supp, min_conf, rules, "rules")
    save_various_constraints_with_sufix(directory_path, "", rules, min_supp, min_conf)


def save_brute_generated_data(base_directory, seed, tags, rule_parameters, game_objects, expected_rules, noise_cover):
    directory_path = create_brute_sub_directory(base_directory, seed, game_objects, expected_rules, noise_cover)
    save_tags(directory_path, tags)
    save_game_objects(directory_path, game_objects)
    write_to_file(f"{directory_path}/rule_parameters.csv", f"{rule_parameters}")
    save_expected_rules(directory_path, expected_rules)
    return directory_path


def save_recommendations(directory_path, recommendations, suffix=''):
    formatted_recommendations = reformat_list(recommendations).replace(", ", "\n").replace(": ", ";")
    write_to_file(f"{directory_path}/recommendations{suffix}.csv", formatted_recommendations)


def save_recommendations_comparison(directory, game_objects, apriori, word2vec, naive_bayes, logistic_regression,
                                    print_console=False):
    text = 'Game Object Id;Apriori;;;Word2Vec;;;Naive Bayes;;;Logistic Regression\n'

    for obj_index in range(len(game_objects)):
        text += f'\n{game_objects[obj_index].id}'

        recommendation_count = max(len(apriori[obj_index]),
                                   len(word2vec[obj_index]),
                                   len(naive_bayes[obj_index]),
                                   len(logistic_regression[obj_index]))

        for i in range(recommendation_count):
            apriori_recommendation = format_recommendation(apriori[obj_index], i)
            word2vec_recommendation = format_recommendation(word2vec[obj_index], i)
            naive_bayes_recommendation = format_recommendation(naive_bayes[obj_index], i)
            logistic_regression_recommendation = format_recommendation(logistic_regression[obj_index], i)
            text += f';{apriori_recommendation};;{word2vec_recommendation};;{naive_bayes_recommendation};;{logistic_regression_recommendation}\n'

    if print_console:
        print(text)

    write_to_file(f"{directory}/recommendations_comparison.csv", text)


def format_recommendation(recommendations, index):
    return f'{recommendations[index]}'.replace(':', ';') if index < len(recommendations) else ';'


def save_power_law_generated_data(base_directory, seed, tags, game_objects, initial_tags_count, iterations, a):
    directory_path = create_power_law_sub_directory(base_directory, seed, game_objects, initial_tags_count, iterations,
                                                    a)
    save_tags(directory_path, tags)
    save_game_objects(directory_path, game_objects)

    return directory_path


def create_power_law_sub_directory(base_directory, seed, game_objects, initial_tags_count, iterations, a):
    a_string = f"{a}".replace('.', '_')
    directory_path = f"{base_directory}/Seed_{seed}_Objects_{len(game_objects)}_InitialTags_{initial_tags_count}_Iterations_{iterations}_A_{a_string}"
    return create_directory_path_if_not_exists(directory_path)
