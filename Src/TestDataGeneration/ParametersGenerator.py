import random
from typing import List

from Src.Entities.Rule import Rule
from Src.Entities.Tag import Tag


def generate_rule_with_constraints(seed, tags: List[Tag], rule_parameters):
    random.seed(seed)
    k_mn = max(rule_parameters.k_mn, 1)
    k_mx = max(min(rule_parameters.k_mx, len(tags) - 1), k_mn)

    k_z = random.randint(k_mn, k_mx)

    premise = random.sample(tags, k_z + 1)
    recommendation = random.choice(premise)
    premise.remove(recommendation)

    support = random.uniform(rule_parameters.s_mn, rule_parameters.s_mx)
    confidence = random.uniform(rule_parameters.c_mn, rule_parameters.c_mx)
    
    support = min(support, confidence)

    return Rule(premise, recommendation, support, confidence)
