import random


def next_seed(seed):
    random.seed(seed)
    return random.randint(-1000000000, 1000000000)
