import random
from Src.Entities.Tag import Tag


def create_tags(seed, max_categories=30, max_names=1000):
    random.seed(seed)

    categories_file = open("tags_categories.txt", "r")
    tag_categories = categories_file.read().split()[:max_categories - 1]
    categories_file.close()

    names_file = open("tags_names.txt", "r")
    tag_names = names_file.read().split()[:max_names - 1]
    names_file.close()

    tags = []

    for name in tag_names:
        category = random.choice(tag_categories)
        tags.append(Tag(category, name))

    return tags
