import random
from Src.TestDataGeneration.GameObjectsCreator import generate_game_objects
from Src.TestDataGeneration.GeneratorUtilities import next_seed
from Src.TestDataGeneration.TagOccurrence import TagOccurrence


class PowerLawTestDataGenerator:
    tags = []
    game_objects_count = 0
    initial_tags_count = 10
    iterations = 0
    a = 0.2,
    initial_tags_per_object = 1

    def generate(self, seed, tags, game_objects_count, initial_tags_count, iterations, a=0.2,
                 initial_tags_per_object=1):

        self.tags = tags
        self.game_objects_count = game_objects_count
        self.initial_tags_count = initial_tags_count
        self.iterations = iterations
        self.a = a
        self.initial_tags_per_object = initial_tags_per_object

        # required_objects = self._compute_max_required_objects_count()
        #
        # if required_objects > game_objects_count:
        #     parameters = f"{iterations} iterations, {initial_tags_count} initial tags count," \
        #                  f" {initial_tags_per_object} initial tags per object, {a} alpha"
        #     raise Exception(
        #         f"PowerLawTestDataGenerator: Invalid input, for {parameters}, required at least {required_objects}")

        game_objects = self._generate_internal(seed)

        return game_objects, []

    def _compute_max_required_objects_count(self):  # add rounding
        max_tags_per_iteration = [self.initial_tags_per_object]

        for i in range(self.iterations - 1):
            new_max = 2 * max_tags_per_iteration[i] + self.a
            max_tags_per_iteration.append(new_max)

        object_from_initial_tags = (self.initial_tags_count - 1) * max_tags_per_iteration[self.iterations - 1]
        return object_from_initial_tags + sum(max_tags_per_iteration)

    def _generate_internal(self, seed):
        game_objects = generate_game_objects(self.game_objects_count)

        seed = next_seed(seed)
        tags_occurrences, seed = self.create_tags_occurrences(seed)

        seed = next_seed(seed)
        game_objects = self.put_tags_on_objects(game_objects, tags_occurrences, seed)

        return game_objects

    def create_tags_occurrences(self, seed):
        chosen_tags = random.sample(self.tags, self.initial_tags_count)
        remaining_tags = [tag for tag in self.tags if tag not in chosen_tags]
        tags_occurrences = [TagOccurrence(tag, self.initial_tags_per_object) for tag in chosen_tags]

        for i in range(self.iterations - 1):
            seed = next_seed(seed)

            for tag_occurrence in tags_occurrences:
                tag_occurrence.occurrences = (1 + random.random()) * tag_occurrence.occurrences + self.a

            new_tag = random.choice(remaining_tags)
            remaining_tags.remove(new_tag)
            tags_occurrences.append(TagOccurrence(new_tag, self.initial_tags_per_object))

        tags_occurrences.sort(key=lambda to: to.occurrences)

        return tags_occurrences, seed

    def put_tags_on_objects(self, game_objects, tags_occurrences, seed):

        seed = next_seed(seed)

        for tag_with_occurrence in tags_occurrences:
            count = tag_with_occurrence.count

            if count > self.game_objects_count:
                print(f"PowerLawGenerator: Required {count} objects, has only {self.game_objects_count}, CLAMP")
                count = self.game_objects_count

            sample = random.sample(game_objects, count)
            seed = next_seed(seed)

            for obj in sample:
                obj.add_tag(tag_with_occurrence.tag)

        return game_objects
