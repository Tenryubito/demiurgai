import random

from Src.TestDataGeneration.GameObjectsCreator import generate_game_objects
from Src.TestDataGeneration.GeneratorUtilities import next_seed
from Src.TestDataGeneration.ParametersGenerator import generate_rule_with_constraints


class BruteTestDataGenerator:

    def generate(self, seed, tags, game_objects_count, R_mn, R_mx, rule_parameters, noise_cover):

        game_objects = generate_game_objects(game_objects_count)

        seed = self.next_seed(seed)
        game_objects, rules = self.project_rules(seed, tags, R_mn, R_mx, game_objects, rule_parameters)

        seed = self.next_seed(seed)
        game_objects = self.add_noise(game_objects, noise_cover, tags)

        return game_objects, rules

    @staticmethod
    def add_noise(game_objects, noise_cover, tags):
        z = int(len(game_objects) * noise_cover)

        for i in range(z):
            random_tag = random.choice(tags)
            random_object = random.choice(game_objects)
            random_object.add_tag(random_tag)

        return game_objects

    def project_rules(self, seed, tags, R_mn, R_mx, game_objects, rule_parameters):
        R = random.randint(R_mn, R_mx)
        rules = []

        for i in range(R):
            seed = next_seed(seed)
            rule = generate_rule_with_constraints(seed, tags, rule_parameters)
            rules.append(rule)

            x = round(len(game_objects) * rule.support)
            y = round(x * rule.confidence)

            objects_with_premise = random.sample(game_objects, x)

            for obj in objects_with_premise:
                obj.add_tags(rule.premise)

            objects_with_recommendation = random.sample(objects_with_premise, y)

            for obj in objects_with_recommendation:
                obj.add_tag(rule.recommendation)

        return game_objects, rules

    def generate_without_interference(self, seed, tags, game_objects_count, R_mn, R_mx, rule_parameters, noise_cover):

        game_objects = generate_game_objects(game_objects_count)

        seed = next_seed(seed)
        game_objects, rules = self.project_rules_without_interference(seed, tags, R_mn, R_mx, game_objects,
                                                                      rule_parameters)

        seed = next_seed(seed)
        game_objects = self.add_noise(game_objects, noise_cover, tags)

        return game_objects, rules

    def project_rules_without_interference(self, seed, tags, R_mn, R_mx, game_objects, rule_parameters):
        R = random.randint(R_mn, R_mx)
        rules = []
        tags_to_use = list(tags)
        object_index = 0

        for i in range(R):
            seed = next_seed(seed)
            rule = generate_rule_with_constraints(seed, tags_to_use, rule_parameters)
            rules.append(rule)

            tags_to_use = [tag for tag in tags_to_use if tag not in rule.premise]
            tags_to_use.remove(rule.recommendation)

            x = round(len(game_objects) * rule.support)
            y = round(x * rule.confidence)

            while x > 0:
                game_objects[object_index].add_tags(rule.premise)
                x -= 1

                if y > 0:
                    game_objects[object_index].add_tag(rule.recommendation)
                    y -= 1

                object_index = (object_index + 1) % len(game_objects)

        return game_objects, rules
