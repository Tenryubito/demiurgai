class TagOccurrence:
    tag = {}
    occurrences = 0

    def __init__(self, tag, occurrence):
        self.tag = tag
        self.occurrences = occurrence

    @property
    def count(self):
        x = self.occurrences
        return int(x + (0.5 if x > 0 else -0.5))

    def __repr__(self):
        return f"{self.tag}: {self.occurrences}"
