from Src.Entities.GameObjectEntity import GameObjectEntity


def generate_game_objects(game_objects_count):
    objects = []
    for i in range(game_objects_count):
        obj = GameObjectEntity(i)
        objects.append(obj)
    return objects
