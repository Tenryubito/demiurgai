import random
from typing import List

from Src.Entities.Tag import Tag
from Src.TestDataGeneration.GameObjectsCreator import generate_game_objects


class SimpleTestDataGenerator:
    tags: List[Tag]

    def __init__(self, tags):
        self.tags = tags

    def generate(self, game_objects_count: int, min_tags_in_first_noise: int = 1, max_tags_in_first_noise: int = 6,
                 rules_count: int = 10, min_tags_per_rule: int = 2, max_tags_per_rule: int = 4,
                 rules_add_depth: int = 1, min_tags_in_second_noise: int = 0, max_tags_in_second_noise: int = 3):
        game_objects = generate_game_objects(game_objects_count)
        game_objects = self.__add_noise(game_objects, min_tags_in_first_noise, max_tags_in_first_noise)

        rules = self.__generate_rules(rules_count, min_tags_per_rule, max_tags_per_rule)
        game_objects = self.__add_tags_from_rules(game_objects, rules, rules_add_depth)

        game_objects = self.__add_noise(game_objects, min_tags_in_second_noise, max_tags_in_second_noise)
        return game_objects, rules

    def __add_noise(self, game_objects, min_tag_per_object, max_tag_per_object):
        for obj in game_objects:
            for tag in random.sample(self.tags, random.randint(min_tag_per_object, max_tag_per_object)):
                obj.add_tag(tag)
        return game_objects

    def __generate_rules(self, count, min_tags_per_rule, max_tags_per_rule):
        rules = []
        for i in range(count):
            sample_len = random.randint(min_tags_per_rule, max_tags_per_rule);
            chosen_tags = random.sample(self.tags, sample_len)
            edge = random.randrange(1, sample_len)

            input_tags = chosen_tags[:edge]
            input_tags.sort()
            output_tags = chosen_tags[edge:]
            output_tags.sort()
            rules.append((input_tags, output_tags))

        return rules

    @staticmethod
    def __add_tags_from_rules(game_objects, rules, rules_add_depth):
        for obj in game_objects:
            try_add_tags = True
            while try_add_tags and rules_add_depth > 0:
                tag_was_added = False
                for rule in rules:
                    if all(tag in obj.tags for tag in rule[0]):
                        for tagToAdd in rule[1]:
                            tag_was_added |= obj.add_tag(tagToAdd)
                try_add_tags = tag_was_added
                rules_add_depth -= 1

        return game_objects
