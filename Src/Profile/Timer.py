import time
from statistics import median


class Timer:
    __last_timestamp: int
    saved_time_spans = []
    memory = 0

    def __init__(self):
        self.__last_timestamp = time.perf_counter()
        self.saved_time_spans = []

    def save_tick(self, label: str):
        current_timestamp = time.perf_counter()
        self.saved_time_spans.append((label, current_timestamp - self.__last_timestamp))
        self.__last_timestamp = current_timestamp

    def __repr__(self):
        total = self.total_time()
        formatted_total = "{0:.6f}".format(total)
        output = f"Total = {formatted_total} s\n"
        i = 1

        for (label, time_span) in self.saved_time_spans:
            time = "{0:.6f}".format(time_span)
            percent = "{0:.3f}".format(time_span * 100 / total)
            output += f"{i}. {label}: {time} [{percent}%]\n"
            i += 1

        return output

    def total_time(self):
        return sum([x[1] for x in self.saved_time_spans])


def merge_timers(timers):
    timer = Timer()
    timer.memory = median([t.memory for t in timers])

    for property_index in range(0, len(timers[0].saved_time_spans)):

        result = median([t.saved_time_spans[property_index][1] for t in timers])

        label = timers[0].saved_time_spans[property_index][0]
        timer.saved_time_spans.append((label, result))

    return timer
