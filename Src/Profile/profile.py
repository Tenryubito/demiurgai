import os
import psutil


def get_process_memory():
    process = psutil.Process(os.getpid())
    return process.memory_info().rss


def profile(func):
    def wrapper(*args, **kwargs):
        mem_before = get_process_memory()
        result = func(*args, **kwargs)
        mem_after = get_process_memory()
        print("{:,}".format(mem_after - mem_before))
        return result, mem_after - mem_before
    return wrapper