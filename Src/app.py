from flask import Flask, request, jsonify
from flask.json import JSONEncoder

from Src.Entities.AprioriRule import filtered_apriori_rules, margin_of_error, is_rule_suitable_for_game_object, \
    apriori_for_game_object
from Src.Entities.GameObjectEntity import GameObjectEntity
from Src.Entities.Recommendation import Recommendation
from Src.Entities.Tag import Tag
from Src.Saving.Saving import create_directory_path_if_not_exists, save_data_with_rules


# DOCUMENTATION: https://docs.google.com/document/d/1jBc_MT-3ufohoNqw1H21777C5bplo3e15dj1BWE0xgA/edit

class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        return obj.__dict__


app = Flask(__name__)
app.json_encoder = CustomJSONEncoder

MIN_SUPP = 0.03
MIN_CONF = 0.6

game_objects = []
tags = []


@app.route('/create-game-object', methods=['GET'])
def create_game_object():
    try:
        game_object_id = len(game_objects)
        game_objects.append(GameObjectEntity(game_object_id))
        return jsonify({"gameObjectId": game_object_id})
    except:
        return jsonify({"gameObjectId": ""})


@app.route('/create-tag', methods=['POST'])
def create_tag():
    try:
        tag_dictionary = request.get_json()
        created_tag = Tag(tag_dictionary["category"], tag_dictionary["name"])
        tags_with_id = [tag for tag in tags if tag.id() == created_tag.id()]

        if len(tags_with_id) == 0:
            tags.append(created_tag)
            return jsonify({"tagId": created_tag.id()})
        else:
            return jsonify({"tagId": tags_with_id[0].id()})
    except:
        return jsonify({"tagId": ""})


@app.route('/add-tag-to-game-object/<int:game_object_id>', methods=['POST'])
def add_tag_to_game_object(game_object_id):
    game_object: GameObjectEntity = game_objects[game_object_id]
    tag = find_tag_from_request()

    if tag is None:
        return jsonify({"errorMessage": "Tag not exists"})

    if tag in game_object.tags:
        return jsonify({"errorMessage": "GameObject already has tag"})

    game_object.add_tag(tag)
    return jsonify({"errorMessage": ""})


def find_tag_from_request():
    try:
        tag_dictionary = request.get_json()
        tag_id = tag_dictionary["tagId"]
        tags_with_id = [tag for tag in tags if tag.id() == tag_id]

        if len(tags_with_id) == 1:
            return tags_with_id[0]
    except:
        pass

    return None


@app.route('/remove-tag-from-game-object/<int:game_object_id>', methods=['POST'])
def remove_tag_from_game_object(game_object_id):
    game_object: GameObjectEntity = game_objects[game_object_id]
    tag = find_tag_from_request()

    if tag is None:
        return jsonify({"errorMessage": "Tag not exists"})

    if tag not in game_object.tags:
        return jsonify({"errorMessage": "GameObject has not got tag"})

    game_object.remove_tag(tag)
    return jsonify({"errorMessage": ""})


@app.route('/recommendation/<int:game_object_id>', methods=['GET'])
def recommendation(game_object_id):
    game_object: GameObjectEntity = game_objects[game_object_id]
    category_win_offset = request.args.get('categoryWinThreshold') or 0.2
    rules = apriori_for_game_object(game_objects, game_object, MIN_SUPP, MIN_CONF, category_win_offset)

    return jsonify({"categoryWinThreshold": category_win_offset, "recommendations": rules})


@app.route('/get-all-game-objects', methods=['GET'])
def get_all_game_objects():
    return jsonify({"gameObjects": game_objects})


@app.route('/get-all-tags', methods=['GET'])
def get_all_tags():
    return jsonify({"tags": tags})


@app.route('/clear-database', methods=['GET'])
def clear_database():
    game_objects.clear()
    tags.clear()
    return jsonify({"errorMessage": ""})


@app.route('/save-database', methods=['GET'])
def save_database():
    directory_path = f"OUTPUT/Database_{len(game_objects)}_Objects_{len(tags)}_Tags";
    create_directory_path_if_not_exists(directory_path);

    filtered_rules = filtered_apriori_rules(game_objects, MIN_SUPP, MIN_CONF);
    save_data_with_rules(directory_path, game_objects, tags, MIN_SUPP, MIN_CONF, filtered_rules)

    return jsonify({"errorMessage": f""})


if __name__ == '__main__':
    app.run(debug=True)
