import gensim

from Src.Entities.Recommendation import Recommendation


def word2vec_rules(game_objects, game_object, number_of_recommendations, size=10, min_count=2, alpha=0.1,
                   min_alpha=0.001, epochs=100, seed=0, window=999, negative=5):
    model = train_word2vec(game_objects, size, min_count, alpha, min_alpha, epochs, seed, window, negative)

    recommendation = word2vec_for_game_object(game_object, number_of_recommendations, model)

    return recommendation, model


def train_word2vec(game_objects, size=15, min_count=2, alpha=0.1, min_alpha=0.001, epochs=100, seed=0, window=999,
                   negative=5):
    transactions = [game_object.tags_only() for game_object in game_objects]
    model = gensim.models.Word2Vec(size=size, min_count=min_count, workers=1, sg=0, window=window, alpha=alpha,
                                   min_alpha=min_alpha, cbow_mean=0, seed=seed, negative=negative)
    model.build_vocab(transactions)
    model.train(transactions, total_examples=model.corpus_count, epochs=epochs)

    return model


def word2vec_for_game_object(game_object, number_of_recommendations, model):
    words = set(game_object.tags_only())
    words = words.intersection(model.wv.vocab)
    if len(words) > 0:
        raw_recommendations = model.wv.most_similar(positive=words, topn=number_of_recommendations)
        return [Recommendation(recommendation[0], recommendation[1]) for recommendation in raw_recommendations]
    return []
