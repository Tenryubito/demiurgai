from typing import Dict


class Tag:
    category: str
    name: str
    values: Dict

    def __init__(self, category, name):
        self.category = category
        self.name = name

    def __repr__(self):
        return self.id()

    def __lt__(self, other):
        return self.category < other.category and self.name < other.name

    def id(self):
        return f"{self.category}_{self.name}"
