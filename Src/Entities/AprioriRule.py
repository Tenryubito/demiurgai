import math

from efficient_apriori import apriori

from Src.Entities.Recommendation import Recommendation


def is_not_trivial(rule):
    premise = rule.lhs
    recommendation = rule.rhs

    for recommendation_tag in recommendation:
        for premise_tag in premise:
            # category_name -> category
            if recommendation_tag in premise_tag:
                return False
    return True


def is_not_coupled(rule):
    premise = rule.lhs
    recommendation = rule.rhs

    for recommendation_tag in recommendation:
        for premise_tag in premise:
            # category -> category_name
            if premise_tag in recommendation_tag:
                return False
    return True


MIN_SUPP = 0.03
MIN_CONF = 0.6


def filtered_apriori_rules(game_objects, min_supp, min_conf, rule_predicate=None):
    rules = apriori_rules(game_objects, min_supp, min_conf)
    return recommendations_from_rules(game_objects, rule_predicate, rules)


def apriori_rules(game_objects, min_supp=MIN_SUPP, min_conf=MIN_CONF, category_to_tag_filtering=True,
                  tag_to_category_filtering=True):
    transactions = [game_object.tags_with_categories() for game_object in game_objects]
    _, rules = apriori(transactions, min_support=min_supp, min_confidence=min_conf)
    rules = [rule for rule in rules if len(rule.rhs) == 1]

    if tag_to_category_filtering:
        rules = [rule for rule in rules if is_not_trivial(rule)]

    if category_to_tag_filtering:
        rules = [rule for rule in rules if is_not_coupled(rule)]

    return rules


def recommendations_from_rules(game_objects, rules, rule_predicate):
    if rule_predicate is not None:
        rules = [rule for rule in rules if rule_predicate(rule)]

    rules.sort(key=lambda rule: (-margin_of_error(rule.support * len(game_objects), rule.confidence)))
    raw_recommendations = merge_proposals(rules)

    return [
        Recommendation(rule.rhs[0], margin_of_error(rule.support * len(game_objects), rule.confidence), rule.lhs) for
        rule in raw_recommendations]


def margin_of_error(supportive_transactions, confidence):
    z = 2.58
    c = confidence
    w = supportive_transactions

    base = (c + z ** 2 / (2 * w)) / (1 + z ** 2 / w)
    error = z / (1 + z ** 2 / w) * math.sqrt((c * (1 - c) / w + z ** 2 / (4 * w ** 2)))
    return base - error


def merge_proposals(rules):
    merged_rules = {}

    for rule in rules:
        if rule.rhs not in merged_rules:
            merged_rules[rule.rhs] = rule

    return merged_rules.values()


def apriori_for_game_object_from_rules(rules, game_objects, game_object, category_win_offset=0.2, k=None):
    recommendations = recommendations_from_rules(game_objects, rules,
                                                 lambda rule: is_rule_suitable_for_game_object(rule, game_object))

    recommendations = filter_weak_categories(recommendations, category_win_offset)
    return recommendations if k is None else recommendations[:k]


def apriori_for_game_object(game_objects, game_object, min_supp=MIN_SUPP, min_conf=MIN_CONF, category_win_offset=0.2):
    recommendations = filtered_apriori_rules(game_objects, min_supp, min_conf,
                                             lambda rule: is_rule_suitable_for_game_object(rule, game_object))

    return filter_weak_categories(recommendations, category_win_offset)


def filter_weak_categories(recommendations, category_win_offset):
    full_tags_rules = [rule for rule in recommendations if rule.is_full_tag]
    return [rule for rule in recommendations if
            is_full_tag_or_better_category(rule, full_tags_rules, category_win_offset)]


def is_full_tag_or_better_category(rule, full_tags_rules, category_win_offset):
    if rule in full_tags_rules:
        return True

    full_tags_with_category = [tag_rule for tag_rule in full_tags_rules if rule.is_category_of(tag_rule)]

    return all(rule.strength > tag_rule.strength + category_win_offset for tag_rule in full_tags_with_category)


def is_rule_suitable_for_game_object(rule, game_object):
    contains_premise = all(game_object.contains(tag_or_category) for tag_or_category in rule.lhs)
    not_contains_recommendation = all(game_object.contains(tag_or_category) is False for tag_or_category in rule.rhs)

    return contains_premise and not_contains_recommendation
