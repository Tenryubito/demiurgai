import matplotlib.pyplot as plt


def prepare_plot():
    fig = plt.figure()
    sub = fig.add_subplot()
    sub.tick_params(axis='x', colors='white')
    sub.tick_params(axis='y', colors='white')
    sub.tick_params(axis='y', colors='white')
