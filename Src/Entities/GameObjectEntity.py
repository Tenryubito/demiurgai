from typing import List


class GameObjectEntity:
    id: int
    tags: List = []

    def __init__(self, object_id):
        self.id = object_id
        self.tags = []

    def add_tag(self, tag):
        if tag not in self.tags:
            self.tags.append(tag)
            return True
        return False

    def remove_tag(self, tag):
        self.tags.remove(tag)

    def add_tags(self, tags):
        for tag in tags:
            self.add_tag(tag)

    def __repr__(self):
        return f"\n{self.id}: {self.tags}"

    def tags_with_categories(self):
        result = [f"{tag}" for tag in self.tags]
        result.extend(self.categories())
        return result

    def tags_only(self):
        result = [f"{tag}" for tag in self.tags]
        return result

    def categories(self):
        result = list()
        result.extend({tag.category for tag in self.tags})
        return result

    def contains(self, tag_id_or_category):
        for tag in self.tags:
            if tag.category == tag_id_or_category or tag.id() == tag_id_or_category:
                return True

        return False
