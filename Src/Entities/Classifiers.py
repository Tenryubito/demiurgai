from bisect import bisect_left
import numpy as np

from Src.Entities.Recommendation import Recommendation


class ClassifierData:
    model = {}
    all_classes = []

    def __init__(self, model, all_classes):
        self.model = model
        self.all_classes = all_classes


def prepare_for_classifier(game_objects):
    transactions = [game_object.tags_with_categories() for game_object in game_objects]
    all_classes = list({premise for premises in transactions for premise in premises})
    all_classes.sort()
    X = []
    Y = []

    for trans in transactions:
        base_row = crate_row(trans, all_classes)
        for tag in trans:
            row = crate_row_for_class(base_row, tag, all_classes)
            X.append(row)
            Y.append(tag)

    return X, Y, all_classes


def crate_row(trans, all_classes):
    row = np.zeros(len(all_classes))

    for c in trans:
        set_tag_flag(row, c, 1, all_classes)

    return row


def set_tag_flag(row, tag, value, all_classes):
    i = bin_search_index(all_classes, tag)
    if i != -1:
        row[i] = value


def bin_search_index(array, x):
    i = bisect_left(array, x)
    if i != len(array) and array[i] == x:
        return i
    else:
        return -1


def crate_row_for_class(base_row, tag, all_classes):
    row = list(base_row)
    set_tag_flag(row, tag, 0, all_classes)

    return row


def create_ranking(Y, trans, all_classes, k):
    # multiple classes
    probabilities = list(Y)

    for premise in trans:
        set_tag_flag(probabilities, premise, -0.001, all_classes)
    ranking = []

    remaining_recommendations_count = min(len(all_classes), k * 2)  # for categories

    while remaining_recommendations_count > 0:
        index = find_index_of_max_value(probabilities)
        ranking.append(Recommendation(all_classes[index], probabilities[index]))
        probabilities[index] = -0.0001
        remaining_recommendations_count -= 1

    ranking = filter_weak_categories(ranking, 0.2)

    return ranking[:k]


def find_index_of_max_value(array):
    max_value = -1
    max_element_index = -1

    for i in range(len(array)):
        element = array[i]
        if element > max_value:
            max_value = element
            max_element_index = i

    return max_element_index


def filter_weak_categories(recommendations, category_win_offset):
    full_tags_rules = [rule for rule in recommendations if rule.is_full_tag]
    return [rule for rule in recommendations if
            is_full_tag_or_better_category(rule, full_tags_rules, category_win_offset)]


def is_full_tag_or_better_category(rule, full_tags_rules, category_win_offset):
    if rule in full_tags_rules:
        return True

    category_rule = rule
    tags_with_category = [tag_rule for tag_rule in full_tags_rules if category_rule.is_category_of(tag_rule)]

    return all(
        category_rule.strength > tag_rule.strength * (1 + category_win_offset) for tag_rule in tags_with_category)
