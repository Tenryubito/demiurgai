class RuleParameters:
    k_mn = 1
    k_mx = 1
    s_mn = 0
    s_mx = 1
    c_mn = 0
    c_mx = 1

    def __init__(self, k_mn=1,
                 k_mx=1,
                 s_mn=0,
                 s_mx=1,
                 c_mn=0,
                 c_mx=1):
        self.k_mn = k_mn
        self.k_mx = k_mx
        self.s_mn = s_mn
        self.s_mx = s_mx
        self.c_mn = c_mn
        self.c_mx = c_mx

    def __repr__(self):
        return f"k_mn: {self.k_mn}\nk_mx: {self.k_mx}\ns_mn: {self.s_mn}\ns_mx: {self.s_mx}\nc_mn: {self.c_mn}\n" \
               f"c_mx: {self.c_mx} "
