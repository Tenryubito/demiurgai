import time
from cmath import sqrt
from statistics import mean, median
import numpy
import xarray
import matplotlib.pyplot as plt

from Src.Saving.Saving import create_directory_path_if_not_exists

Ks = [1, 3, 5]  # Precision at 1, 3, 5


def save_plot(base_directory, isel, measure_name, plot_title, game_objects_counts, legend):
    plt.clf()
    plt.plot(isel)
    plt.xticks(range(len(game_objects_counts)), game_objects_counts)
    plot_title = plot_title
    plt.suptitle(plot_title)
    plt.legend(legend)
    plt.xlabel("Objects count")

    #   NCG
    plt.ylabel(measure_name)
    # directory = f'{base_directory_path}/NCG_with_interference'
    directory = f'{base_directory}/{measure_name}'
    create_directory_path_if_not_exists(directory)
    #   Time
    # plt.ylabel("Time [s]")
    # directory = f'{base_directory_path}/Times'
    # create_directory_path_if_not_exists(directory)
    #
    plt.savefig(f'{directory}/{measure_name}_To_ObjectsCountAndNoiseCover_{plot_title}.png')


def run_experiment_and_save_plot(base_directory, seeds_count, analyze, plot_title):
    seeds = range(seeds_count)
    # game_objects_counts = [100, 1000]
    game_objects_counts = [10, 50, 100, 500, 1000]
    # noise_covers = [0, 1, 3, 5]
    noise_covers = [0, 0.1, 0.5, 1, 3, 5, 7]

    data = xarray.DataArray(dims=['game_objects_count', 'noise_cover', 'measure'],
                            data=numpy.ndarray((len(game_objects_counts), len(noise_covers), 6)))

    for go_count_index in range(0, len(game_objects_counts)):
        for no_cover_index in range(0, len(noise_covers)):
            go_count = game_objects_counts[go_count_index]
            no_cover = noise_covers[no_cover_index]
            evaluations = []
            times = []
            # print(f"Objects: {go_count}, NoiseCover: {no_cover}, Seed: 0-{SEEDS_COUNT}")

            for seed in seeds:
                start_timestamp = time.perf_counter()

                score = analyze(seed, no_cover, go_count)

                evaluations.append(score[Ks[2]])
                times.append(time.perf_counter() - start_timestamp)

            data[go_count_index, no_cover_index, 0] = mean(evaluations)
            data[go_count_index, no_cover_index, 1] = median(evaluations)
            data[go_count_index, no_cover_index, 2] = numpy.std(evaluations) / sqrt(len(evaluations))
            data[go_count_index, no_cover_index, 3] = mean(times)
            data[go_count_index, no_cover_index, 4] = median(times)
            data[go_count_index, no_cover_index, 5] = numpy.std(times) / sqrt(len(times))

    # print(noise_cover_to_rules_count)

    legend = [f"{nc} noise cover" for nc in noise_covers]

    ncg_isel = data.isel(measure=1)
    save_plot(base_directory, ncg_isel, "NCG_median", plot_title, game_objects_counts, legend)

    time_isel = data.isel(measure=3)
    save_plot(base_directory, time_isel, "Time", plot_title, game_objects_counts, legend)
