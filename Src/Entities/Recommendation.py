class Recommendation:
    proposal = []
    strength = 0
    premise = []

    def __init__(self, proposal, strength, premise=[]):
        self.proposal = proposal
        self.strength = strength
        self.premise = premise

    def __repr__(self):
        return f"{self.proposal}: {round(self.strength, 5)}"

    @property
    def is_full_tag(self):
        return "_" in self.proposal

    def is_category_of(self, tag_rule):
        return tag_rule.proposal[0].startswith(self.proposal[0])
