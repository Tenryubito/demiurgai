from Src.Entities.Classifiers import prepare_for_classifier, crate_row, ClassifierData, create_ranking
from sklearn.linear_model import LogisticRegression


def logistic_regression_rules(game_objects, game_object, alpha=1., k=10):
    classifier_data = train_logistic_regression(game_objects, alpha)
    recommendation = logistic_regression_for_game_object(game_object, k, classifier_data)

    return recommendation, classifier_data


def train_logistic_regression(game_objects, C=1., solver='lbfgs'):
    X, Y, all_classes = prepare_for_classifier(game_objects)

    model = LogisticRegression(random_state=0, C=C, solver=solver)
    model.fit(X, Y)

    return ClassifierData(model, all_classes)


def logistic_regression_for_game_object(game_object, k, classifier_data):
    trans = game_object.tags_with_categories()
    X = [crate_row(trans, classifier_data.all_classes)]
    Y = classifier_data.model.predict_proba(X)[0]

    return create_ranking(Y, trans, classifier_data.all_classes, k)

    # one class
    # return model.predict(X)[0]
