from Src.Entities.AprioriRule import margin_of_error


class Rule:
    premise = []
    recommendation = []
    support = 0
    confidence = 0

    def __init__(self, premise, recommendation, support, confidence):
        self.premise = premise
        self.recommendation = recommendation
        self.support = support
        self.confidence = confidence

    def __repr__(self):
        return f"\n{self.premise} -> {self.recommendation} supp: {round(self.support, 4)} conf: {round(self.confidence, 4)}"

    def strength(self, game_object_count):
        return margin_of_error(self.support * game_object_count, self.confidence)
