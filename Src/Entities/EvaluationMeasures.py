from typing import List

from Src.Entities.GameObjectEntity import GameObjectEntity
from Src.Entities.Rule import Rule


def evaluate_normal_cumulative_gain(ks: List[int], expected_rules: List[Rule], game_objects_count,
                                    compute_recommendations):
    expected_rules.sort(key=lambda r: -r.support)

    scores = {k: 0 for k in ks}
    rankings = []

    for rule in expected_rules:
        test_pseudo_object = GameObjectEntity(-1)
        test_pseudo_object.add_tags(rule.premise)

        ranking = compute_recommendations(test_pseudo_object, max(ks))
        rankings.append(ranking)

        for k in ks:
            if f"{rule.recommendation}" in [r.proposal for r in ranking[:k]]:
                scores[k] += rule.support

    max_score = sum([expected_rule.support for expected_rule in expected_rules])

    result = {k: scores[k]/max_score for k in ks}

    return result, rankings
