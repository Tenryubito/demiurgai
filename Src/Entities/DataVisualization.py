from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

sns.set_style("darkgrid")


def visualize_model(model, expected_rules=[], learning_rate=100, perplexity=15):
    vocab = list(model.wv.vocab)
    X = model[vocab]
    tsne = TSNE(random_state=0, n_components=2, learning_rate=learning_rate, perplexity=perplexity).fit_transform(X)

    expected_proposals = {f'{r.recommendation}' for r in expected_rules}
    expected_premises = {f'{p}' for r in expected_rules for p in r.premise}

    df = pd.DataFrame({
        'x': [x for x in tsne[:, 0]],
        'y': [y for y in tsne[:, 1]],
        'color': ['red' if word in expected_proposals else 'blue' if word in expected_premises else 'grey' for word in
                  vocab]

    }, index=vocab)

    fig, _ = plt.subplots()
    fig.set_size_inches(9, 9)

    # Basic plot
    p1 = sns.regplot(data=df,
                     x="x",
                     y="y",
                     fit_reg=False,
                     marker="o",
                     scatter_kws={'s': 40,
                                  'facecolors': df['color']
                                  }
                     )

    # Adds annotations one by one with a loop
    for line in range(df.shape[0]):
        p1.text(df["x"][line],
                df['y'][line],
                '  ' + vocab[line].title(),
                horizontalalignment='left',
                verticalalignment='bottom', size='small',
                color=df['color'][line],
                weight='normal'
                ).set_size(5)

#   alternative
#     fig = plt.figure()
#     ax = fig.add_subplot(1, 1, 1)
#
#     ax.scatter(df['x'], df['y'], marker=".")
#
#     for word, pos in df.iterrows():
#         ax.annotate(word, pos, fontsize=3)
