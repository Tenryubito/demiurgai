from sklearn.naive_bayes import BernoulliNB

from Src.Entities.Classifiers import prepare_for_classifier, crate_row, ClassifierData, create_ranking


def naive_bayes_bernoulli_rules(game_objects, game_object, alpha=1., k=10):
    classifier_data = train_naive_bayes_bernoulli(game_objects, alpha)
    recommendation = naive_bayes_bernoulli_for_game_object(game_object, k, classifier_data)

    return recommendation, classifier_data


def train_naive_bayes_bernoulli(game_objects, alpha=1.):
    X, Y, all_classes = prepare_for_classifier(game_objects)

    model = BernoulliNB(alpha=alpha)
    model.fit(X, Y)

    return ClassifierData(model, all_classes)


def naive_bayes_bernoulli_for_game_object(game_object, k, classifier_data):
    trans = game_object.tags_with_categories()
    X = [crate_row(trans, classifier_data.all_classes)]
    Y = classifier_data.model.predict_proba(X)[0]

    return create_ranking(Y, trans, classifier_data.all_classes, k)

    # one class
    # return model.predict(X)[0]


def debug_naive_bayes(classifier_data, game_objects, game_object):
    print('=' * 30)
    for tag in game_object.tags_only():
        print(tag)
    print('=' * 30)

    trans = game_object.tags_only()
    X = [crate_row(trans)]

    # multiple classes
    probabilities = list(classifier_data.model.predict_proba(X)[0])

    transactions = [game_object.tags_only() for game_object in game_objects]
    all_tags = list({tag for tans in transactions for tag in tans})
    all_tags.sort()

    i = 0
    all_probs = []
    for p in probabilities:
        all_probs.append((all_tags[i], p))
        i += 1

    all_probs.sort(key=lambda tuple: -tuple[1])

    for p in all_probs:
        prob = "{0:.4f}".format(p[1])
        print(f'{p[0]} {prob}')
